package uk.co.channele.schema;

/**
 * Created by steven on 07/07/17.
 */
public class CreativeWork extends Thing {

    String award;
    String keywords;

    public String getAward() {
        return award;
    }

    public void setAward(String award) {
        this.award = award;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

}
