package uk.co.channele.schema;

import java.util.Date;

/**
 * Created by steven on 07/07/17.
 */
public class CourseInstance extends Thing {

    String courseMode;
    Person attendee;
    Date doorTime;
    Date startDate;
    Date endDate;
    String inLanguage;
    PostalAddress location;
    Integer maximumAttendeeCapacity;
    CreativeWork workPerformed;

    public String getCourseMode() {
        return courseMode;
    }

    public void setCourseMode(String courseMode) {
        this.courseMode = courseMode;
    }

    public Person getAttendee() {
        return attendee;
    }

    public void setAttendee(Person attendee) {
        this.attendee = attendee;
    }

    public Date getDoorTime() {
        return doorTime;
    }

    public void setDoorTime(Date doorTime) {
        this.doorTime = doorTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getInLanguage() {
        return inLanguage;
    }

    public void setInLanguage(String inLanguage) {
        this.inLanguage = inLanguage;
    }

    public PostalAddress getLocation() {
        return location;
    }

    public void setLocation(PostalAddress location) {
        this.location = location;
    }

    public Integer getMaximumAttendeeCapacity() {
        return maximumAttendeeCapacity;
    }

    public void setMaximumAttendeeCapacity(Integer maximumAttendeeCapacity) {
        this.maximumAttendeeCapacity = maximumAttendeeCapacity;
    }

}
